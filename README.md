# Install LibreOffice



## Install LibreOffice in Kali Linux

To make it easy for you to get started with GitLab, here's a list of recommended next steps.
Kali Linux is one of the most famous distros for hacking and penetration testing, but it does not come with office software. Thus, we can install LibreOffice on Kali Linux using only one terminal command, if needed.

After Apache OpenOffice, LibreOffice is another widely used free and open-source office. In the free category, LibreOffice is one of the finest alternatives to Microsoft office. It includes all of the modules which we need to complete our document-related activities.

From word processing to a spreadsheet and the development of presentations, all aspects are covered. In addition to many other formats, Microsoft formats like DOC and DOCX are also supported.


## What is LibreOffice?

LibreOffice is a free and powerful office suite that is used by millions of people all over the world. It is a simple interface and feature-rich tool which help us to unleash our creativity and increase our productivity.

LibreOffice contains a various application which makes it the most versatile free and open-source office suite on the market:

Writer (word processing) = MS Word
Calc (spreadsheets) = Microsoft Excel
Impress (presentations) = Microsoft PowerPoint
Draw (vector graphics and flowcharts).
Base (databases), = MS Access
Math (formula editing).


## Supported Documents Extension by LibreOffice

LibreOffice is compatible with a wide range of document formats like Microsoft Word (.doc, .docx), Excel (.xls, .xlsx), PowerPoint (.ppt, .pptx) and Publisher.

However, LibreOffice goes much farther with its native support for a modern and open standard, the Open Document Format (ODF).


## Install LibreOffice in Kali Linux Using Terminal
We have to follow the following steps to install LibreOffice in Kali Linux using terminal:

1. First, we have to open the terminal.

2. Next, we have to update Kali Linux with the help of the following command:

```sudo apt-get update```

┌──(hunter㉿Hunter)-[~]
└─$ sudo apt-get update 
[sudo] password for hunter: 
Hit:1 https://dl.google.com/linux/chrome/deb stable InRelease
Get:2 http://kali.download/kali kali-rolling InRelease [41.2 kB]
Get:3 http://kali.download/kali kali-rolling/main amd64 Packages [19.4 MB]
Get:4 http://kali.download/kali kali-rolling/main amd64 Contents (deb) [45.8 MB] 
[...]

When we run the apt-get update command, we have to take care of one thing, which is, we have added the kali Linux repository. If we forget to add repository source in the source.list file then we might face some problem,

3. Now, by executing the following command, we can install the LibreOffice in Kali Linux:


```sudo apt-get install libreoffice```



┌──(hunter㉿Hunter)-[~]
└─$ sudo apt-get install libreoffice
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
The following packages were automatically installed and are no longer required:
  gir1.2-gtksource-3.0 gir1.2-javascriptcoregtk-4.0 gir1.2-soup-2.4
  gir1.2-webkit2-4.0 gobject-introspection king-phisher libblockdev-crypto2
  libblockdev-fs2 libblockdev-loop2 libblockdev-part-err2 libblockdev-part2
  l[..]

  4. Run LibreOffice. In order to run it, we have to go to All Application and search for our recently installed open-source office suite. As its icon appears, run it to begin working with our document-related tasks.


